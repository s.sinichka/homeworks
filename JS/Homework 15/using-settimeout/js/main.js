let addZero = number => number < 10 ? `0${number}` : `${number}`;

let startTimer = () => {
  timerID = setTimeout(function countTime() {
    ms++;
    if (ms === 100) {
      s++;
      ms = 0;
    }
    if (s === 60) {
      m++;
      s = 0;
    }
    if (m === 60) m = 0;
    updateTimer();
    timerID = setTimeout(countTime, 10);
  }, 10);

  startBtn.style.display = 'none';
  pauseBtn.style.display = 'inline-block';
};

let updateTimer = () => {
  document.getElementById('m-seconds').innerText = `${addZero(ms)}`;
  document.getElementById('seconds').innerText = `${addZero(s)}`;
  document.getElementById('minutes').innerText = `${addZero(m)}`;
};

let pauseTimer = () => {
  clearInterval(timerID);
  startBtn.style.display = 'inline-block';
  pauseBtn.style.display = 'none';
};

let clearTimer = () => {
  pauseTimer();
  m = s = ms = 0;
  updateTimer();
};

let m = 0;
let s = 0;
let ms = 0;

let timerID = null;

const startBtn = document.getElementById('start-btn');
const pauseBtn = document.getElementById('pause-btn');
const clearBtn = document.getElementById('clear-btn');

startBtn.addEventListener('click', startTimer);
pauseBtn.addEventListener('click', pauseTimer);
clearBtn.addEventListener('click', clearTimer);
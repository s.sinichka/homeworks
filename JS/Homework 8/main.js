function getValue(obj, path) {
  path = path.split('.');

  for (let key of path) {
    if (typeof obj[key] === 'object') {
      obj = obj[key];
      continue;
    }
    return obj[key];
  }
}

function includesValue(obj, value, pathsArray) {
  value = value.toLowerCase();
  let searchObj = obj;

  for (let path of pathsArray) {
    let foundValue = getValue(searchObj, path).toLowerCase()
    if (foundValue.includes(value))
      return true;

    searchObj = obj;
  }
  return false;
}

function filterCollection(filteredArray, KeywordsString, findAll, ...pathsArray) {
  const keywords = KeywordsString.split(' ');

  return filteredArray.filter(elem => {
    if (findAll) {
      for (let keyword of keywords)
        if (!includesValue(elem, keyword, pathsArray))
          return false;

      return true;
    }

    for (let keyword of keywords)
      if (includesValue(elem, keyword, pathsArray))
        return true;

    return false;
  });
}

// TEST === TEST === TEST === TEST === TEST === TEST === TEST //

let movies = [
  {
    director: 'Robert Zemekis',
    country: 'US',
    language: 'ENG',
    actors: {
      male: 'Johnny Depp',
      female: 'Amy Adams'
    }
  },
  {
    director: 'Robert Rodrigues',
    country: 'US',
    language: 'ENG',
    actors: {
      male: 'Tom Hanks',
      female: 'Selena Gomes'
    }
  },
  {
    director: 'Robert Zemekis',
    country: 'US',
    language: 'ENG',
    actors: {
      male: 'Johnny Depp',
      female: 'Jennifer Lawrence'
    }
  },
  {
    director: 'Robert Zemekis',
    country: 'US',
    language: 'ENG',
    actors: {
      male: 'Johnny Depp',
      female: 'Jennifer Lawrence'
    }
  },
  {
    director: 'Robert Zemekis',
    country: 'US',
    language: 'ENG',
    actors: {
      male: 'Tom Hanks',
      female: 'Amy Adams'
    }
  },
  {
    director: 'Robert Zemekis',
    country: 'US',
    language: 'ENG',
    actors: {
      male: 'Morgan Freeman',
      female: 'Amy Adams'
    }
  }
];


let filtered = filterCollection(movies, 'adAms frEEman Zemekis', true, 'actors.male', 'actors.female', 'director');
console.log(filtered);
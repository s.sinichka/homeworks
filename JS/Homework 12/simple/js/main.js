const fisrtBtn = document.getElementById('first-btn');
fisrtBtn.addEventListener('click', pubInputs);


function pubInputs() {
  const parent = document.getElementById('main-content');
  parent.innerHTML = `<input type="text" placeholder="Диаметр (px)" class="diameter-input" id="diameter-input"><input type="text" class="color-input" id="color-input" placeholder="Цвет (CSS)"><a href="#" class="draw-btn" id="draw-btn">Нарисовать</a>`;

  const drawBtn = document.getElementById('draw-btn');
  drawBtn.addEventListener('click', drawCircle);
}

function drawCircle() {
  const diameterInp = document.getElementById('diameter-input');
  const colorInp = document.getElementById('color-input');

  const diameter = +diameterInp.value;
  const color = colorInp.value;
  console.log(diameter);
  console.log(color);

  const parent = document.getElementById('main-content');
  parent.innerHTML = `<div class="circle" id="circle" style="width:${diameter}px;height:${diameter}px;background-color:${color}"></div>`;
}